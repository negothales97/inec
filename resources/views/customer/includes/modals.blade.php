<!-- Modal -->
<div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container">
                    <h3 class="title"></h3>
                    <p>
                        <strong>Objetivo geral:</strong> <span class="objetivo"></span>
                    </p>
                    <p>
                        <strong>Público alvo:</strong> <span class="publico"></span>
                    </p>
                    <p>
                        <strong>Pré-requisitos:</strong> <span class="requisitos"></span>
                    </p>
                    <p>
                        <strong>Duração:</strong> <span class="duracao"></span>
                    </p>
                    <p class="display-none">
                        <strong>Carga horária:</strong> 90 horas.
                    </p>
                    <p>
                        <strong>Estrutura curricular:</strong>
                        <ul class="estrutura-curricular">
                        </ul>
                    </p>
                    <p>
                        <strong style="color: #006600;" class="desconto"></strong>
                    </p>
                    <p>
                        <strong>Forma de pagamento:</strong> <span class="valor"> </span>

                    </p>
                    <p>
                        <strong>Taxa de matrícula:</strong> <span class="taxa"></span>
                    </p>
                    <div class="row">
                        <div class="col-sm-4">
                            <button class="btn-modal"
                                onclick="window.location.href='{{route('subscription')}}';">INSCREVA-SE</button>
                        </div>
                        <div class="col-sm-4 offset-sm-4">
                            <button class="btn-modal" id="btn-red" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="professionalModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container">
                    <h3 class="title"></h3>
                    <p>
                        <strong>Objetivo geral:</strong> <span class="objetivo"></span>
                    </p>
                    <p>
                        <strong>Público alvo:</strong> <span class="publico"></span>
                    </p>
                    <p>
                        <strong>Pré-requisitos:</strong> <span class="requisitos"></span>
                    </p>
                    <p>
                        <strong>Duração:</strong> <span class="duracao"></span>
                    </p>
                    <p class="display-none">
                        <strong>Carga horária:</strong> 90 horas.
                    </p>
                    <p>
                        <strong style="color: #006600;" class="desconto"></strong>
                    </p>
                    <p>
                        <strong>Forma de pagamento:</strong> <span class="valor"> </span>
                    </p>
                    <p>
                        <strong>Taxa de matrícula:</strong> <span class="taxa"></span>
                    </p>
                    <div class="row">
                        <div class="col-sm-4">
                            <button class="btn-modal"
                                onclick="window.location.href='{{route('subscription')}}';">INSCREVA-SE</button>
                        </div>
                        <div class="col-sm-4 offset-sm-4">
                            <button class="btn-modal" id="btn-red" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="workshopModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container">
                    <h3 class="title"></h3>
                    <p>
                        <strong>Objetivo geral:</strong> <span class="objetivo"></span>
                    </p>
                    <p>
                        <strong>Público alvo:</strong> <span class="publico"></span>
                    </p>
                    <p>
                        <strong>Pré-requisitos:</strong> <span class="requisitos"></span>
                    </p>
                    <p>
                        <strong>Duração:</strong> <span class="duracao"></span>
                    </p>
                    <p class="display-none">
                        <strong>Carga horária:</strong> 90 horas.
                    </p>
                    <p>
                        <strong style="color: #006600;" class="desconto"></strong>
                    </p>
                    <p>
                        <strong>Investimento total:</strong> <span class="investimento"></span>
                    </p>
                    <p>
                        <strong>Forma de pagamento:</strong> <span class="valor"> </span>

                    </p>
                    <div class="row">
                        <div class="col-sm-4">
                            <button class="btn-modal"
                                onclick="window.location.href='{{route('subscription')}}';">INSCREVA-SE</button>
                        </div>
                        <div class="col-sm-4 offset-sm-4">
                            <button class="btn-modal" id="btn-red" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@yield('modals')