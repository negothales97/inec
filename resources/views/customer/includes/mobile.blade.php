<div id="menu-mobile" class="animated">
	<div id="mainmenu-mobile" style="display: block;">
		<div class="row">
			<div class="col">
				<p class="title-item-menu">Menu</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<ul>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Cursos</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="https://portal.uniasselvi.com.br/graduacao" target="_blank">Graduação</a>
							<a class="dropdown-item" href="https://portal.uniasselvi.com.br/posgraduacao" target="_blank">Pós Graduação On-line</a>
							<a class="dropdown-item" href="{{route('pos-graduation.presencial')}}">Pós Graduação Presencial</a>
							<a class="dropdown-item" href="{{route('professionalizing')}}">Profissionalizante Presencial</a>
							<a class="dropdown-item" href="https://eadaulas.com/inecubatuba/loja_virtual/">Profissionalizante EAD</a>
							<a class="dropdown-item" href="https://eadaulas.com/inecubatuba/loja_virtual/cursos.php?id=INFORM%C3%81TICA">Informática</a>
							<a class="dropdown-item" href="{{route('subscription')}}">EJA</a>
							<a class="dropdown-item" href="https://www.unienber.com/inec/">Mestrado e Doutorado</a>
							<a class="dropdown-item" href="https://www.cpetcursotecnico.com.br/ead/ineubatuba">Curso Técnico</a>
							<a class="dropdown-item" href="{{route('subscription')}}">Extensão Universitária</a>
							<a class="dropdown-item" href="{{route('workshop')}}">Seminário</a>
						</div>
					</li>
					<!-- <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Projetos</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="ipc">IPC - Ubatuba</a>
							<a class="dropdown-item" href="#">Revista Educação</a>
						</div>
					</li> -->
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Professores</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="http://www.inecontinuada.com.br/portaldoprofessor/" target="_blank">Portal do Professor</a>
							<a class="dropdown-item" href="{{route('professor')}}">Quadro de professores</a>
						</div>
					</li>
					<li>
						<a href="dados-da-empresa">Dados da empresa</a>
					</li>
					<li>
						<a href="onde-estudar">Onde estudar</a>
					</li>
					<li>
						<a href="empresa">O Inec</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="index#parceiros">Parceiros</a>
					</li>
					<li class="nav-item" id="contact-us">
						<a class="nav-link" href="fale-conosco">Fale conosco</a>
					</li>
					<li>
						<button onclick="window.open('https://portaldoaluno.inecontinuada.com.br/');"><img id="btn-aluno" src="{{asset('images/icons/aluno.png')}}" alt="Icon">Portal do Aluno</button>
					</li>
					<li>
						<button onclick="window.open('https://portaldoprofessor.inecontinuada.com.br/');"><img id="btn-professor" src="{{asset('images/icons/professor.png')}}" alt="Icon">Portal do Professor</button>
					</li>
					<li>
						<button onclick="window.open('https://portaldoconsultor.inecontinuada.com.br/');"><img id="btn-consultor" src="{{asset('images/icons/consultor.png')}}" alt="Icon">Portal do Consultor</button>
					</li>
					<li>
						<button onclick="window.open('https://portaldaempresa.inecontinuada.com.br/');"><img id="btn-empresas" src="{{asset('images/icons/empresa.png')}}" alt="Icon">Portal das Empresas</button>
					</li>
					<li>
						<div class="whatsb">
							<a href="https://api.whatsapp.com/send?phone=551238326890"><img src="{{asset('images/icons/whatsb.png')}}" alt="Icon">(12) 3832-6890</a>
						</div>
					</li>
					<!-- <li>
						<div class="phone-red">
							<a href="https://api.whatsapp.com/send?phone=551238326890"><img src="{{asset('images/icons/telefone.png')}}" alt="Icon"></a>
						</div>
					</li> -->
				</ul>
			</div>
		</div>
	</div>
</div>