<header class="header-top">
	<div id="top">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="animated fadeInUp delay-1s">
						<li><a href="{{route('data.company')}}">Dados da empresa</a></li>
						<li><a href="{{route('study')}}">Onde estudar</a></li>
						<li><a href="{{route('company')}}">O inec</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<a href="{{route('home')}}">
					<img id="logo" src="{{asset('images/logov4.png')}}" alt="Inec"
						class="float-left">
				</a>
				<ul class="nav justify-content-end">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
							aria-haspopup="true" aria-expanded="false">Cursos</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="https://portal.uniasselvi.com.br/graduacao" target="_blank">Graduação</a>
							<a class="dropdown-item" href="https://portal.uniasselvi.com.br/posgraduacao" target="_blank">Pós Graduação On-line</a>
							<a class="dropdown-item" href="{{route('pos-graduation.presencial')}}">Pós Graduação Presencial</a>
							<a class="dropdown-item" href="{{route('professionalizing')}}">Profissionalizante Presencial</a>
							<a class="dropdown-item" href="https://eadaulas.com/inecubatuba/loja_virtual/">Profissionalizante EAD</a>
							<a class="dropdown-item" href="https://eadaulas.com/inecubatuba/loja_virtual/cursos.php?id=INFORM%C3%81TICA">Informática</a>
							<a class="dropdown-item" href="{{route('subscription')}}">EJA</a>
							<a class="dropdown-item" href="https://www.unienber.com/inec/">Mestrado e Doutorado</a>
							<a class="dropdown-item" href="https://www.cpetcursotecnico.com.br/ead/ineubatuba">Curso Técnico</a>
							<a class="dropdown-item" href="{{route('subscription')}}">Extensão Universitária</a>
							<a class="dropdown-item" href="{{route('workshop')}}">Seminário</a>
						</div>
					</li>
					<!-- <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
							aria-haspopup="true" aria-expanded="false">Projetos</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="ipc">IPC - Ubatuba</a>
							<a class="dropdown-item" href="#">Revista Educação</a>
						</div>
					</li> -->
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
							aria-haspopup="true" aria-expanded="false">Professores</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="http://www.inecontinuada.com.br/portaldoprofessor/"
								target="_blank">Portal do Professor</a>
							<a class="dropdown-item" href="{{route('professor')}}">Quadro de professores</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('home')}}#partners">Parceiros</a>
					</li>
					<li class="nav-item" id="contact-us">
						<a class="nav-link" href="{{route('contact')}}">Fale conosco</a>
					</li>
				</ul>
				<div id="btn-toggle-menu">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
		</div>
	</div>
</header>

@include('customer.includes.mobile')

<div id="overlay-menu" class="animated fadeIn"></div>