<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Og tags -->
<meta property="og:url" content="https://inecontinuada.com.br/">
<meta property="og:title" content="INEC" />
<meta property="og:type" content="website"/>
<meta property="og:description" content="Integração em Educação Continuada" />
<meta property="og:image" content="https://inecontinuada.com.br/images/og-image.png" />
<meta property="og:image:width" content="800">
<meta property="og:image:height" content="600">

<title>Inec</title>
<meta name="description" content="INEC">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="canonical" href="https://inecontinuada.com.br/">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;0,800;1,400&display=swap" rel="stylesheet">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pretty-checkbox/3.0.0/pretty-checkbox.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="{{asset('lightbox2-master/dist/js/lightbox-plus-jquery.min.js')}}"></script>
<link href="{{asset('lightbox2-master/dist/css/lightbox.css')}}" rel="stylesheet">

<link rel="stylesheet" href="{{asset('css/app.css?v=1.0.3')}}">