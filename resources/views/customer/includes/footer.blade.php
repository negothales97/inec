<footer>
	<div class="lgpd" id="cookie">
		<div class="container">
			Ao navegar neste site, você aceita os cookies que usamos para melhorar sua experiência. 
			<button id="btnClose" onclick="document.getElementById('cookie').style.display='none';">
				Entendi
			</button>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-12">			
				<img src="{{asset('images/logov3.png')}}" alt="Inec">
				<div id="icons-footer" class="icons-footer-desktop">
					<p>
						<br>
						<strong>SIGA-NOS</strong>
					</p>
					<a href="#"><img src="{{asset('images/icons/facebook.png')}}" alt="Facebook"></a>
					<a href="#"><img src="{{asset('images/icons/instagram.png')}}" alt="Instagram"></a>
					<a href="#"><img src="{{asset('images/icons/linkedin.png')}}" alt="Linkedin"></a>
				</div>
			</div>
			<div class="col-12 col-md">
				<strong>PORTAIS</strong>
				<ul>
					<li><a href="http://www.inecontinuada.com.br/portaldoaluno/" target="_blank">Portal do Aluno</a></li>
					<li><a href="http://www.inecontinuada.com.br/portaldoconsultor/" target="_blank">Portal do Consultor</a></li>
					<li><a href="http://www.inecontinuada.com.br/portaldoprofessor/" target="_blank">Portal do Professor</a></li>
					<li><a href="http://www.inecontinuada.com.br/portaldaempresa/" target="_blank">Portal da Empresa</a></li>
				</ul>
			</div>
			<div class="col-12 col-md">
				<strong>CURSOS</strong>
				<ul>
					<li><a href="{{route('subscription')}}">Extensão</a></li>
					<li><a href="https://portal.uniasselvi.com.br/graduacao" target="_blank">Graduação</a></li>
					<li><a href="https://portal.uniasselvi.com.br/posgraduacao/sp/ubatuba/educacao-especial-transtorno-do-espectro-autista?place=polo-uniasselvi-ceu-ubatuba&interest=4" target="_blank">Pós Graduação</a></li>
					<li><a href="{{route('professionalizing')}}">Profissionalizante Presencial</a></li>
					<li><a href="https://eadaulas.com/inecubatuba/loja_virtual/">Profissionalizante EAD</a></li>
					<li><a href="https://eadaulas.com/inecubatuba/loja_virtual/cursos.php?id=INFORM%C3%81TICA">Informática</a></li>
					<li><a href="{{route('subscription')}}">EJA</a></li>
					<li><a href="https://www.unienber.com/inec/">Mestrado e Doutorado</a></li>
					<li><a href="https://www.cpetcursotecnico.com.br/ead/inecubatuba">Curso Técnico</a></li>
					<li><a href="{{route('subscription')}}">Extensão Universitária</a></li>
					<li><a href="{{route('workshop')}}">Workshops e Palestras</a></li>
				</ul>
			</div>
			<div class="col-12 col-md">
				<strong>LINKS ÚTEIS</strong>
				<ul id="last-child-mobile">
					<li><a href="{{url('empresa')}}">O INEC</a></li>
					<li><a href="{{url('dados-da-empresa')}}">Dados da Empresa</a></li>
					<li><a href="{{url('onde-estudar')}}">Onde Estudar</a></li>
					<li><a href="{{url('fale-conosco')}}">Fale Conosco</a></li>
				</ul>
				<div id="icons-footer" class="icons-footer-mobile">
					<p>
						<br>
						<strong>SIGA-NOS</strong>
					</p>
					<a href="#"><img src="{{asset('images/icons/facebook.png')}}" alt="Facebook"></a>
					<a href="#"><img src="{{asset('images/icons/instagram.png')}}" alt="Instagram"></a>
					<a href="#"><img src="{{asset('images/icons/linkedin.png')}}" alt="Linkedin"></a>
				</div>
			</div>
		</div>
	</div>
	<div id="credits">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>© 2020 | Inec.  Todos os direitos reservados.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
