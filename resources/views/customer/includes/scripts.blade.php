<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
	integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
	integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>

<script type="text/javascript">
	const setData =(idModal, curso) => {
		$(`#${idModal} .title`).text(curso.nome);
        $(`#${idModal} .objetivo`).text(curso.objetivo);
        $(`#${idModal} .publico`).text(curso.publico);
        $(`#${idModal} .requisitos`).text(curso.requisitos);
	}
	const setDesconto = (idModal, curso) => {
		let valor = (curso.valor/curso.duracao);
		if(curso.hasDesconto === true){
			let valorDesconto =  valor -valor* (curso.desconto/ 100);
			$(`#${idModal} .desconto`).text(`Desconto especial de ${curso.desconto} %`);
			$(`#${idModal} .valor`).html(`de ${curso.duracao} X R$ ${(floatToReal(valor))} por apenas <strong> ${curso.duracao} X R$ ${floatToReal(valorDesconto)} mensais </strong>`);
		}else{
			$(`#${idModal} .valor`).html(`${curso.duracao} X R$ ${floatToReal(valor)}`);
		}
	}
	function floatToReal(n, c, d, t) {
    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" :
        "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n -
        i).toFixed(c).slice(2) : "");
}

	$('.main').on('click', function(){
        let curso = $(this).data('curso');
        setData('courseModal', curso);
		$('#courseModal .duracao').text(`${curso.duracao} meses`);
		let html = '';
		curso.estrutura.forEach((item, key) =>html += `<li>- ${item.nome} </li>`);
		$('#courseModal .estrutura-curricular').html(html);
		setDesconto('courseModal', curso);
		$('#courseModal .taxa').text(`R$ ${floatToReal(curso.inscricao)}`);

		$('#courseModal').modal('show');
    });
	$('.professional').on('click', function(){
		let curso = $(this).data('curso');
		let duracao = curso.duracao == 1 ? '1 mês' :  `${curso.duracao} meses`;

        setData('professionalModal', curso);
		$('#professionalModal .duracao').text(duracao);

		setDesconto('professionalModal', curso);
		
		$('#professionalModal .taxa').text(`R$ ${floatToReal(curso.inscricao)}`);
		$('#professionalModal').modal('show');
    });
	$('.workshop').on('click', function(){
		let curso = $(this).data('curso');
        setData('workshopModal', curso);
		$('#workshopModal .duracao').text(`${curso.total} horas`);
		setDesconto('workshopModal', curso);
		$('#workshopModal .investimento').text(`R$ ${floatToReal(curso.inscricao)}`);
		$('#workshopModal').modal('show');
    });
	
		$(document).ready(function(){
			$(".products-carousel").owlCarousel({
				loop:true,
				margin:12,
				nav:true,
				dots: false,
				autoplay: true,
				autoplayTimeout: 3000,
				smartSpeed: 800,
				navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
				responsive:{
					992:{
						items:3
					},
					480:{
						items:2
					},
					0:{
						items:1
					}
				}
			});

			$(".posts-carousel").owlCarousel({
				loop:true,
				margin:12,
				nav:true,
				dots: false,
				navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
				responsive:{
					992:{
						items:3
					},
					480:{
						items:2
					},
					0:{
						items:1
					}
				}
			});

			$(".customers-carousel").owlCarousel({
				loop:true,
				margin:12,
				nav:true,
				dots: false,
				navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
				responsive:{
					992:{
						items:5
					},
					480:{
						items:3
					},
					0:{
						items:2
					}
				}
			});
		});

</script>