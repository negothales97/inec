<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        @include('customer.includes.head')
    </head>

    <body class="{{request()->is('/') ? 'index' : 'no-index'}}">

        @include('customer.includes.header')

        @yield('content')

        @include('customer.includes.footer')
        @include('customer.includes.modals')
        
        @include('customer.includes.scripts')

        @yield('scripts')

    </body>

</html>
