<section class="buttons">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <button onclick="window.open('http://www.inecontinuada.com.br/portaldoaluno/', '_blank');"><img
                        id="btn-aluno" src="{{asset('images/icons/aluno.png')}}" alt="Icon">Portal do Aluno</button>
                <button onclick="window.open('http://www.inecontinuada.com.br/portaldoprofessor/', '_blank');"><img
                        id="btn-professor" src="{{asset('images/icons/professor.png')}}" alt="Icon">Portal do Professor</button>
                <button onclick="window.open('http://www.inecontinuada.com.br/portaldoconsultor/', '_blank');"><img
                        id="btn-consultor" src="{{asset('images/icons/consultor.png')}}" alt="Icon">Portal do Consultor</button>
                <button onclick="window.open('http://www.inecontinuada.com.br/portaldaempresa/', '_blank');"><img
                        id="btn-empresas" src="{{asset('images/icons/empresa.png')}}" alt="Icon">Portal das Empresas</button>
            </div>
            <div class="col-sm-4">
                <div class="whatsb">
                    <a href="https://api.whatsapp.com/send?phone=5512991817059" target="_blank"><img
                            src="{{asset('images/icons/whatsb.png')}}" alt="Icon">(12) 99181-7059</a>
                </div>
                <div class="whatsb" style="margin-right: 15px;">
                    <a href="https://api.whatsapp.com/send?phone=551238326890" target="_blank"><img
                            src="{{asset('images/icons/whatsb.png')}}" alt="Icon">(12) 3832-6890</a>
                </div>
            </div>
        </div>
    </div>
</section>