<div class="pos-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <p>Realize sua inscrição em um de nossos cursos</p>
            </div>
            <div class="col-sm-2">
                <button class="btn-blue" onclick="window.location.href='{{route('subscription')}}';">
                    <div id="border-btn"></div>INSCREVA-SE
                </button>
            </div>
        </div>
    </div>
</div>