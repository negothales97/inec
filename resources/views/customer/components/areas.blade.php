<div class="row">
    @php
    $numOfCols = 2;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    @endphp
    @forelse ($areas as $key => $cursos)
    <div class="col-sm-<?= $bootstrapColWidth; ?>">
        <h3>{{$key}}</h3>
        @foreach ($cursos as $curso)
        <div class="row">
            <div class="col-sm-12">
                <div class="box-course-page {{$clazz}}" data-toggle="modal" data-curso="{{$curso}}">
                    <img src="{{asset('images/icons/board.png')}}" alt="Icon">
                    <strong>{{$curso->nome}}</strong>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @php
    $rowCount++;
    @endphp
    @if($rowCount % $numOfCols == 0)
</div>
<div class="row">
    @endif
    @empty
    <div class="col-sm-12">
        <h3>Nenhum curso disponível no momento. Em breve, novidades!</h3>
    </div>
    @endforelse
</div>