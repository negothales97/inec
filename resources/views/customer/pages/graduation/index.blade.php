@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent
<div class="slider">		
    <div class="banner-one with-span" id="banner-cursos">
        <div class="container">
            <div class="text-content">
                <strong>GRADUAÇÃO</strong>
                <span>EAD</span>
            </div>
        </div>
    </div>
</div>	

<section class="content" id="courses">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Cursos oferecidos em parceria com a UNINOVE</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box-course-page" onclick="window.open('https://www.uninove.br/ead/', '_blank');">
                            <img src="images/icons/board.png" alt="Icon">
                            <strong>MATRÍCULAS ABERTAS! SAIBA MAIS</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent
@endsection