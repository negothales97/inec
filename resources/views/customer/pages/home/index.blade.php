@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent
<div class="slider">
    <div class="banner-one" id="banner-home">
        <div class="container">
            <div class="text-content">
                <strong>ENSINO DE QUALIDADE</strong>
                <span>PARA TODOS</span>
                <button class="btn-blue" onclick="window.location.href='/fale-conosco';"><div id="border-btn"></div>ESTUDE CONOSCO</button>
            </div>
        </div>
    </div>
</div>
@component('customer.components.pos-banner')
@endcomponent
<section class="content" id="course">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Cursos</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Extensão</strong>
                    <p>Proporciona o acesso as mais novas informações, oferecendo conhecimento técnico, teórico e prático...</p>
                    <button onclick="window.location.href='{{route('subscription')}}';">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Graduação</strong>
                    <p>Em parceria com a UNIASSELVI, oferecemos cursos de ensino a distância...</p>
                    <button onclick="window.open('https://portal.uniasselvi.com.br/graduacao', '_blank');">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Pós Graduação</strong>
                    <p>Em parceria com duas instituições de ensino, oferecemos em duas opções: presencial e a...</p>
                    <button onclick="window.open('https://portal.uniasselvi.com.br/posgraduacao/sp/ubatuba/educacao-especial-transtorno-do-espectro-autista?place=polo-uniasselvi-ceu-ubatuba&interest=4', '_blank');">Saiba mais</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Curso Técnico</strong>
                    <p>Querendo conquistar seu novo emprego? Venha se preparar com a gente! Escolha um  dos cursos...</p>
                    <button onclick="window.location.href='{{route('professionalizing')}}';">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Informática</strong>
                    <p>Aumente suas chances de conquistar um novo emprego na era da tecnologia. Cursos com duração...</p>
                    <button onclick="window.location.href='https://eadaulas.com/inecubatuba/loja_virtual/cursos.php?id=INFORM%C3%81TICA';">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Workshops e Palestras</strong>
                    <p>Com os mais variados temas e áreas de atuação para você e sua empresa. Venha aprender mais sobre o assunto...</p>
                    <button onclick="window.location.href='{{route('workshop')}}';">Saiba mais</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Profissionalizante EAD</strong>
                    <p style="width: 100%;"></p>
                    <button onclick="window.location.href='https://eadaulas.com/inecubatuba/loja_virtual/';">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Profissionalizante Presencial</strong>
                    <p style="width: 100%;"></p>
                    <button onclick="window.location.href='https://www.inecontinuada.com.br/profissionalizantes';">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>EJA</strong>
                    <p style="width: 100%;"></p>
                    <button onclick="window.location.href='{{route('subscription')}}';">Saiba mais</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Doutorado</strong>
                    <p style="width: 100%;"></p>
                    <button onclick="window.location.href='https://www.unienber.com/inec/';">Saiba mais</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-course">
                    <div id="border-box"></div>
                    <strong>Mestrado</strong>
                    <p style="width: 100%;"></p>
                    <button onclick="window.location.href='https://www.unienber.com/inec/';">Saiba mais</button>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-sm-12">
                <h3>Projetos</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="box-project-home">
                    <img src="{{asset('images/projetos/projeto1.jpg')}}" alt="Imagem do projeto">
                    <div class="content-project">
                        <strong>IPC - Ubatuba<br>Integração Profissional Continuada</strong>
                        <p>Com o objetivo de selecionar e capacitar jovens entre 15 e 19 anos para empresas...</p>
                        <a href="{{url('ipc')}}">Saiba mais</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-project-home">
                    <img src="{{asset('images/projetos/projeto2.jpg')}}" alt="Imagem do projeto">
                    <div class="content-project">
                        <strong>Revista Científica<br>Educação</strong>
                        <p>Com o objetivo de selecionar e capacitar jovens entre 15 e 19 anos para empresas...</p>
                        <a href="#">Saiba mais</a>
                    </div>
                </div>
            </div>
        </div> -->
        <div id="partners">
            <div class="row">
                <div class="col">
                    <h3>Parceiros</h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="customers-carousel owl-carousel">
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/famesc.png')}}" alt="unienber">
                        </div>
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/uniasselvi.jpeg')}}" alt="Uniasselvi">
                        </div>
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/educamais.jpeg')}}" alt="Centro Educa Mais">
                        </div>
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/saoluis.jpg')}}" alt="São Luis">
                        </div>
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/unifacvest.jpg')}}" alt="Unifacvest">
                        </div>
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/cpet.jpg')}}" alt="CPET">
                        </div>
                        <div class="box-customer">
                            <img src="{{asset('images/parceiros/unienber.jpeg')}}" alt="unienber">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
