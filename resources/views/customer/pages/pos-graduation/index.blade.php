@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')
@component('customer.components.buttons')
@endcomponent

<div class="slider">
    <div class="banner-one with-span" id="banner-cursos">
        <div class="container">
            <div class="text-content">
                <strong>PÓS</strong>
                <span>GRADUAÇÃO</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="courses">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3>Presencial</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-course-page" data-toggle="modal" data-target="#presencialModal">
                            <img src="images/icons/board.png" alt="Icon">
                            <strong>COM AULAS PRESENCIAIS</strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h3>EAD</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-course-page" data-toggle="modal" data-target="#eadModal">
                            <img src="images/icons/board.png" alt="Icon">
                            <strong>A DISTÂNCIA UTILIZANDO O AVA</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('content')
@endsection
@section('modals')

<!-- Modal -->
<div class="modal fade" id="presencialModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container">
                    <h3>Presencial</h3>
                    <p>
                        <strong>Certificado:</strong> Emitido por instituições reconhecidas e autorizadas pelo MEC para
                        oferecer seus respectivos cursos.<br>
                        O INEC oferece cursos de Pós-Graduação Presenciais de duas instituições:
                        <img src="images/logo-famesc.png" alt="Logo">
                    </p>
                    <p>
                        <strong>Professores:</strong> Todos Especialistas, Mestres ou Doutores, de acordo com as
                        exigências do MEC.
                    </p>
                    <p>
                        <strong>Avaliação:</strong> Feita em sala de aula e eventualmente, se o professor considerar
                        necessário, também através de trabalho a ser desenvolvido fora da sala de aula.
                    </p>
                    <p>
                        <strong>Duração:</strong> A partir de 12 meses
                    </p>
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn-modal" onclick="window.location.href='{{route('pos-graduation.presencial')}}';">PREFIRO
                                PRESENCIAL</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="eadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container">
                    <h3>EAD</h3>
                    <p>
                        <strong>Certificado:</strong> Emitido pela UNINOVE (Universidade Nove de Julho), reconhecida e
                        autorizadas pelo MEC para oferecer seus respectivos cursos. Os cursos são modulares, portanto,
                        ao término de cada módulo você receberá uma certificação que melhora continuamente sua
                        qualificação para o mercado de trabalho.<br>
                        <img src="images/logo-uninove.png" alt="Logo">
                    </p>
                    <p>
                        <strong>As aulas:</strong> É utilizada a mais moderna plataforma de ensino a distância o AVA
                        (Ambiente Virtual de Aprendizagem), acessada através do computador. Você também pode utilizar os
                        computadores disponíveis nas Unidades Avançadas da UNINOVE.
                    </p>
                    <p>
                        <strong>Professores:</strong> Todos Especialistas, Mestres ou Doutores, de acordo com as
                        exigências do MEC.
                        <p>
                            <strong>Avaliação:</strong> Existirão avaliações feitas dentro do AVA durante todo o curso e
                            ocorrerá uma prova presencial, que será aplicada em apenas um sábado ao final do curso,
                            conforme cronograma a ser encaminhado para cada aluno.
                        </p>
                        <p>
                            <strong>Duração:</strong> A partir de 12 meses
                        </p>
                        <div class="row">
                            <div class="col-sm-6 offset-sm-3">
                                <button class="btn-modal"
                                    onclick="window.open('https://www.uninove.br/pos-graduacao/', '_blank');">PREFIRO
                                    EAD</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection