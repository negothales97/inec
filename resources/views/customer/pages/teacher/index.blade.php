@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent
<div class="slider">
    <div class="banner-one" id="banner-professores">
        <div class="container">
            <div class="text-content">
                <strong>PROFESSORES</strong>
            </div>
        </div>
    </div>
</div>

<section class="content" id="professores">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Quadro de Professores Convidados</h3>
                @foreach($professores as $professor)
                <div class="box-course-page box-teacher bananal animated fadeInDown">
                    <strong>{{$professor->nome}}</strong>
                    <p>{{$professor->titulo->nome}}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent
@endsection