@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent
<div class="slider">
    <div class="banner-one with-span" id="banner-fale-conosco">
        <div class="container">
            <div class="text-content">
                <strong>FALE</strong>
                <span>CONOSCO</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="fale-conosco">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h3>Formulário</h3>
                @if(session()->has('success'))
                <div class="row">
                    <div class="col-sm-12">
                    
                        <div class="alert alert-success" role="alert">
                            Mensagem enviada com sucesso. Fale conosco por Whatsapp: <a href="https://api.whatsapp.com/send?phone=551238326890" target="_blanl">(12) 3832-6890</a>
                        </div>
                    
                    </div>
                </div>
                @endif
                <form action="{{route('contact.send')}}" method="post">
                    @csrf
                    <div class="box-form">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="name">Nome</label>
                                    <input type="text" name="name" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="email">E-mail</label>
                                    <input type="email" name="email" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="celphone">Celular</label>
                                    <input type="text" name="celphone" inputmode="numeric" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="phone">Tel. Fixo</label>
                                    <input type="text" name="phone" inputmode="numeric" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="city">Cidade</label>
                                    <input type="text" name="city" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="state">Estado</label>
                                    <select name="state" required>
                                        <option selected disabled>Selecione...</option>
                                        <option>Acre</option>
                                        <option>Alagoas</option>
                                        <option>Amapá</option>
                                        <option>Amazonas</option>
                                        <option>Bahia</option>
                                        <option>Ceará</option>
                                        <option>Distrito Federal</option>
                                        <option>Espírito Santo</option>
                                        <option>Goiás</option>
                                        <option>Maranhão</option>
                                        <option>Mato Grosso</option>
                                        <option>Mato Grosso do Sul</option>
                                        <option>Minas Gerais</option>
                                        <option>Pará</option>
                                        <option>Paraíba</option>
                                        <option>Paraná</option>
                                        <option>Pernambuco</option>
                                        <option>Piauí</option>
                                        <option>Rio de Janeiro</option>
                                        <option>Rio Grande do Norte</option>
                                        <option>Rio Grande do Sul</option>
                                        <option>Rondônia</option>
                                        <option>Roraima</option>
                                        <option>Santa Catarina</option>
                                        <option>São Paulo</option>
                                        <option>Sergipe</option>
                                        <option>Tocantins</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="message">Mensagem</label>
                                    <textarea name="message">Mensagem para o polo de {{request('polo')}}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <button type="submit">ENVIAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-sm-3">
                <h3>Contato</h3>
                <div class="box-form" id="box-right">
                    <ul>
                        <li>
                            <div class="whatsb">
                                <a href="https://api.whatsapp.com/send?phone=551238326890"><img src="{{asset('images/icons/whatsb.png')}}" alt="Icon">(12) 3832-6890</a>
                            </div>
                        </li>
                        <li>
                            <div class="whatsb">
                                <a href="https://api.whatsapp.com/send?phone=5512991817059"><img src="{{asset('images/icons/whatsb.png')}}" alt="Icon">(12) 99181-7059</a>
                            </div>
                        </li>
                        <li>
                            <div class="place">
                                <a href="#"><img src="{{asset('images/icons/place.png')}}" alt="Icon">R DOM JOAO III,
                                    197 -
                                    CENTRO<br>
                                    UBATUBA - SP</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent
@endsection
@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
			$(".occupation-carousel").owlCarousel({
				loop:true,
				margin:12,
				nav:true,
				dots: false,
				navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
				responsive:{
					992:{
						items:5
					},
					480:{
						items:3
					},
					0:{
						items:2
					}
				}
			});
		});
</script>
@endsection