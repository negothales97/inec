@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent

<div class="slider">
    <div class="banner-one with-span" id="banner-inscricao">
        <div class="container">
            <div class="text-content">
                <strong>REALIZE</strong>
                <span>SUA INSCRIÇÃO</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="inscricao">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Preencha os dados abaixo</h3>
            </div>
        </div>
        <form action="{{route('subscription.send')}}" method="post">
            @csrf

            @if(session()->has('success'))
            <div class="row">
                <div class="col-sm-10 offset-sm-1">
                
                    <div class="alert alert-success" role="alert">
                        Inscrição realizada com sucesso.
                    </div>
                
                </div>
            </div>
            @endif
            
            <div class="row">
                <div class="col-sm-10 offset-sm-1">
                    <div class="box-form">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="name">Nome completo*</label>
                                    <input type="text" name="name" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="type">Tipo de curso*</label>
                                    <select name="type" required>
                                        <option selected disabled>Selecione...</option>
                                        <option>Extensão</option>
                                        <option>Graduação</option>
                                        <option>Pós Graduação</option>
                                        <option>Profissionalizante Presencial</option>
                                        <option>Profissionalizante EAD</option>
                                        <option>Informática</option>
                                        <option>EJA</option>
                                        <option>Mestrado e Doutorado</option>
                                        <option>Curso Técnico</option>
                                        <option>Extensão Universitária</option>
                                        <option>Workshops e Palestras</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="nameCourse">Nome do curso*</label>
                                    <input type="text" name="nameCourse" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="celphone">Celular*</label>
                                    <input type="text" name="celphone" inputmode="numeric" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="email">E-mail*</label>
                                    <input type="email" name="email" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="message">Mensagem</label>
                                    <textarea name="message"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <button type="submit">REALIZAR CADASTRO</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <small>Assinalados com (*) são obrigatórios</small>
                </div>
            </div>
        </form>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent

@endsection
@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        $(".occupation-carousel").owlCarousel({
            loop:true,
            margin:12,
            nav:true,
            dots: false,
            navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
            responsive:{
                992:{
                    items:5
                },
                480:{
                    items:3
                },
                0:{
                    items:2
                }
            }
        });
    });
</script>

@endsection