@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent

<div class="slider">
    <div class="banner-one" id="banner-inec">
        <div class="container">
            <div class="text-content">
                <strong>O INEC</strong>
            </div>
        </div>
    </div>
</div>

<section class="content" id="company">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3>O INEC</h3>
                <div class="box-company">
                    <p>
                        O INEC – Integração em Educação Continuada surgiu da experiência de 30 anos dos fundadores na
                        área da Educação como Professores, Coordenadores, Vice-Diretores, Diretores, Supervisores e ATP
                        – Assistentes Técnico-Pedagógicos na rede pública de ensino.<br><br>

                        Com esta vasta experiência, e identificando a carência de diversas regiões do país de cursos de
                        qualidade ofertados por universidades conceituadas, este grupo resolveu criar em 2008 o
                        INEC.<br><br>

                        O INEC concretiza parcerias entre Faculdades, Centros Universitários e Universidades com cursos
                        regulamentados pela Resolução CES/CNE no. 1, de 3 de abril de 2007 que estabelece normas para o
                        funcionamento de cursos de graduação e pós-graduação.<br><br>

                        Atendendo à portaria do MEC 328/2005, todos os cursos de pós-graduação Lato Sensu oferecidos
                        pelo INEC estão regularmente registrados junto aos órgãos competentes.<br><br>

                        O INEC possui uma variada oferta de cursos de Informática, Profissionalizantes, Extensão
                        Universitária, Graduação e Pós-Graduação.<br><br>

                        Todos os nosso cursos de Graduação e Pós-Graduação são devidamente reconhecidos pelo MEC que
                        seguem um sofisticado e prático sistema de ensino, com qualidade e flexibilidade.
                    </p>
                </div>
            </div>
            <div class="col-sm-6">
                <h3>Depoimentos de alunos e ex-alunos</h3>
                <div class="box-company">
                    <p>
                        " Meu nome é Michelle Germano do Nascimento, sou formada em sistemas de Informação e ministro
                        aulas de informática a 11 anos para jovens, adultos e idosos. Meu último trabalho foi aulas de
                        informática para pessoas com deficiência e com isso serem encaminhados para o mercado de
                        trabalho. Por esse motivo precisei fazer o curso de pós graduação em Educação especial com
                        ênfase em Deficiência intelectual. Aconteceu algo muito importante não apenas na área
                        profissional mas como pessoa, pois através das aulas, fui me identificando pois descobri que
                        tenho um pai com deficiência intelectual através desse curso. Fui percebendo que os maiores
                        conflitos familiares eram porque meu pai não tinha total compreensão cognitiva para agir com
                        atitudes corretas quando contrariado em uma conversa simples. Comecei a observa - lo e analisar
                        seu comportamento que era de acordo com assuntos abordados em sala de aula. Ele precisava de
                        ajuda e ninguém o entendia, descobri que os motivos de brigas com todos da família sem
                        necessidade aparente era chamado de surtos porque ele era contrariado. Com todas as informações
                        recebidas no curso de pós graduação consegui ajudar meu pai, com isso ele passou a se sentir
                        mais confortado pois antes sentia as indiferenças ao redor. Expliquei que não tem diagnóstico e
                        que aparentemente mesmo aparentando ser uma pessoa '"normal" porém seu cognitivo precisa ser
                        estimulado e compreendido. Após isso algumas coisas mudaram não foi um mar de rosas mas nos
                        ensinou a ver com outros olhos e mais humanos, como uma pessoa que merece respeito, compreensão
                        e ser ouvida mesmo com todas as suas limitações.
                        Ah esqueci de mencionar aí que quando meu pai era novo ele caiu de cabeça no chão porque um
                        cavalo o derrubou e com isso ele estudou na APAE anos. "
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

@component('customer.components.pos-banner')
@endcomponent
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $(".occupation-carousel").owlCarousel({
            loop:true,
            margin:12,
            nav:true,
            dots: false,
            navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
            responsive:{
                992:{
                    items:5
                },
                480:{
                    items:3
                },
                0:{
                    items:2
                }
            }
        });
    });
</script>

@endsection