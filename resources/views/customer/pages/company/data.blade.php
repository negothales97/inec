@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent

<div class="slider">
    <div class="banner-one with-span" id="banner-dados-empresa">
        <div class="container">
            <div class="text-content">
                <strong>DADOS</strong>
                <span>DA EMPRESA</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="dados-empresa">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Razões sociais que compõe o INEC</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p><strong>INEC - Integração em Educação Continuada</strong> sob CNPJ: 10.143.454/0001-75</p>
            </div>
            <div class="col-sm-6">
                <p><strong>Localização:</strong><br>
                    Rua Dom João III, 197 - Centro<br>
                    CEP: 11680-000 - Ubatuba - SP<br>
                    Telefone: (12) 3832-6890
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.6365456491676!2d-45.07424448520975!3d-23.437492962914252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cd52262a1b08ad%3A0xda3ee67a2d3fe6db!2sR.%20Dom%20Jo%C3%A3o%20III%2C%20197%20-%20Centro%2C%20Ubatuba%20-%20SP%2C%2011680-000!5e0!3m2!1spt-BR!2sbr!4v1590504541064!5m2!1spt-BR!2sbr"
                    width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
            </div>
        </div>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent
@endsection