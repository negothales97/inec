@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent

<div class="slider">
    <div class="banner-one with-span" id="banner-cursos">
        <div class="container">
            <div class="text-content">
                <strong>CURSOS</strong>
                <span>PROFISSIONALIZANTES</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="courses">
    <div class="container">
        @component('customer.components.areas', ['areas' => $areas, 'clazz' => 'professional'])
        @endcomponent
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent
@endsection