@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent
<div class="slider">
    <div class="banner-one with-span" id="banner-onde-estudar">
        <div class="container">
            <div class="text-content">
                <strong>ONDE</strong>
                <span>ESTUDAR</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="onde-estudar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @foreach ($polos as $polo)

                <h3 class="toggle-polo" id="{{$polo->id}}">{{$polo->nome}} <i
                        class="fa fa-angle-down" aria-hidden="true"></i></h3>
                <div class="box-course-page box-address {{$polo->id}} animated fadeInDown">
                    <strong>{{$polo->nome}}</strong>
                    <p>{{"{$polo->endereco}, {$polo->numero}. - {$polo->bairro}"}}</p>
                    <a href="fale-conosco?polo={{$polo->estado}}. - {{$polo->cidade}}">Contato</a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent
@endsection
@section('scripts')

<script type="text/javascript">
    $('.toggle-polo').on('click', function(e){
    e.preventDefault();
    let id = $(this).attr('id');
    $(`.${id}`).toggle('slow');
    let icon =$(this).find("i");
    icon.toggleClass("fa-angle-down fa-angle-up");
});
</script>
@endsection