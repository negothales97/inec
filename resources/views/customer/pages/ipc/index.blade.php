@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')

@component('customer.components.buttons')
@endcomponent
<div class="slider">
    <div class="banner-one with-span" id="banner-ipc">
        <div class="container">
            <div class="text-content">
                <strong>PROJETOS</strong>
                <span>IPC - UBATUBA</span>
            </div>
        </div>
    </div>
</div>

<section class="content" id="ipc">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>INTEGRAÇÃO PROFISSIONAL CONTINUADA</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="box-project">
                    <p>
                        O objetivo do projeto “Integração Profissional Continuada” é selecionar e capacitar jovens entre
                        15 e 19 anos para as empresas do município onde elas atuam.<br><br>

                        Fazer a integração entre as empresas da cidade e os jovens estudantes do INEC através do portal
                        da empresa e do portal do aluno, ambos no site do INEC.
                        Para participar, o jovem interessado deve aguardar a abertura de um processo seletivo. O
                        processo seletivo ocorre duas vezes por ano. Essas datas são divulgadas no comércio local e em
                        locais onde existe o maior número de jovens concentrados.<br><br>

                        Ao participar do projeto, os dados do jovem ficam armazenados e disponíveis para que as empresas
                        parceiras do INEC tenham acesso e eventuamente entrem em contato com esses jovens para possíveis
                        entrevistas de emprego. Isso ocorre de acordo com a necessidade de cada empresa.
                    </p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-project">
                    <p>
                        Aos participantes do projeto, será oferecido um treinamento que visa a qualificação
                        profissional, com base nas necessidades atuais da região, preparando estes jovens para
                        entrevistas de emprego, ensinando a elaborar um currículo corretamente e instruindo os jovens
                        sobre as diversas áreas existentes dentro de uma empresa e como trabalhar em cada uma delas.
                        Trata-se de um treinamento completo e composto por diversos módulos.<br><br>

                        A participação do jovem no projeto, não implica necessariamente na sua contratação por qualquer
                        empresa participante do projeto. Porém, afirmamos que as empresas parceiras do INEC procuram e
                        tem preferência em entrevistar e contratar jovens qualificados, interessados e que constem em
                        nosso banco de dados de currículos, chamado de Currículo ON-LINE.<br><br>

                        Com isso pretendemos sempre oferecer uma excelente qualificaçõo profissional e disponibilizar os
                        jovens já treinados e preparados para o mercado de trabalho, cada dia mais competitivo.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@component('customer.components.pos-banner')
@endcomponent

@endsection
@section('scripts')
    
	<script type="text/javascript">
		$(document).ready(function(){
			$(".occupation-carousel").owlCarousel({
				loop:true,
				margin:12,
				nav:true,
				dots: false,
				navText: ["<img src='images/nav-left.png'>","<img src='images/nav-right.png'>"],
				responsive:{
					992:{
						items:5
					},
					480:{
						items:3
					},
					0:{
						items:2
					}
				}
			});
		});
	</script>
@endsection