<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/portaldoaluno', function(){
    return redirect('https://portaldoaluno.inecontinuada.com.br');
});

Route::get('/portaldoconsultor', function(){
    return redirect('https://portaldoconsultor.inecontinuada.com.br');
});


Route::get('/portaldoprofessor', function(){
    return redirect('https://portaldoprofessor.inecontinuada.com.br');
});


Route::get('/portaldaempresa', function(){
    return redirect('https://portaldaempresa.inecontinuada.com.br');
});

Route::get('/sistema', function(){
    return redirect('https://sistema.inecontinuada.com.br');
});


Route::group(['namespace' => 'Customer'], function(){
    Route::get('/', 'HomeController@index')->name('home');
    
    Route::get('dados-da-empresa', 'PageController@dataCompany')->name('data.company');
    Route::get('onde-estudar', 'PageController@study')->name('study');
    Route::get('empresa', 'PageController@company')->name('company');
    
    Route::get('extensao', 'CourseController@extension')->name('extension');
    Route::get('graduacao', 'CourseController@graduation')->name('graduation');
    Route::get('pos-graduacao', 'CourseController@posGraduation')->name('pos-graduation');
    Route::get('pos-graduacao-presencial', 'CourseController@presencial')->name('pos-graduation.presencial');
    Route::get('profissionalizantes', 'CourseController@professionalizing')->name('professionalizing');
    Route::get('informatica', 'CourseController@computing')->name('computing');
    Route::get('workshop', 'CourseController@workshop')->name('workshop');
    
    Route::get('professores', 'ProfessorController@index')->name('professor');
    Route::get('fale-conosco', 'ContactController@index')->name('contact');
    Route::post('fale-conosco', 'ContactController@send')->name('contact.send');
    Route::get('inscricao', 'SubscriptionController@index')->name('subscription');
    Route::post('inscricao', 'SubscriptionController@send')->name('subscription.send');
    
});

Route::view('ipc','customer.pages.ipc.index');
