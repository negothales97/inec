<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Polo extends Model
{
    protected $table = 'tb_polos';

    protected $fillable = [
        'id',
        'ativo',
        'cadastro',
        'alteracao',
        'nome',
        'endereco',
        'numero',
        'complemento',
        'bairro',
        'estado',
        'cidade',
        'cep',
        'tel_com_1',
        'tel_com_2',
        'n_salas',
        'tipo_pagto',
        'valor',
        'banco',
        'agencia',
        'conta',
        'pagto_obs',
        'favorecido',
        'cpf',
        'cnpj',
        'nome_resp',
        'rg_resp',
        'cpf_resp',
        'telefone_resp',
        'email_resp',
    ];
}
