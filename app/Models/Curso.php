<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'tb_cursos';

    protected $fillable = [
        'id',
        'ativo',
        'site',
        'data_cadastro',
        'tipo',
        'area',
        'nome',
        'parceiro',
        'duracao',
        'carga_horaria',
        'valor',
        'inscricao',
        'desconto',
        'objetivo',
        'publico',
        'requisitos',
        'material',
        'cartao',
        'cartao_parcelas',
        'cartao_valor_total',
    ];

    public function area()
    {
        return $this->belongsTo('App\Models\AreaCurso', 'id', 'area');
    }
}
