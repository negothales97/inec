<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TituloProf extends Model
{
    protected $table = 'tb_titulos_prof';

    protected $fillable =[
        'id',
        'nome',
        'valor',
    ];
}
