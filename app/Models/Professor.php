<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $table ='tb_professores';
    protected $fillable =[
        'id',
        'ativo',
        'senha',
        'data_cadastro',
        'alteracao',
        'ult_acesso',
        'nome',
        'rg',
        'cpf',
        'data_nascimento',
        'sexo',
        'civil',
        'endereco',
        'numero',
        'complemento',
        'bairro',
        'estado',
        'cidade',
        'cep',
        'banco',
        'agencia',
        'conta',
        'pagto_obs',
        'favorecido',
        'favorecido_cpf',
        'email',
        'tel1',
        'tel2',
        'tel3',
        'escolaridade',
        'nome_curso',
        'ano_conclusao',
        'instituicao',
        'rg_doc',
        'cpf_doc',
        'certidao_doc',
        'foto_doc',
        'diploma_doc',
        'comp_end_doc',
        'historico_doc',
        'docs_url',
        'atualizado',
        'obs',
        'autorizacao',
    ];

    public function titulo()
    {
        return $this->belongsTo('App\Models\TituloProf', 'escolaridade', 'id');
    }
}
