<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaCurso extends Model
{
    protected $table = 'tb_area_curso';

    protected $fillable = [
        'id',
        'nome'
    ];

    public function cursos()
    {
        return $this->hasMany('App\Models\Curso', 'area', 'id');
    }
}
