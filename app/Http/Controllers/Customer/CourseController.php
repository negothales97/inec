<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Curso;

class CourseController extends Controller
{
    protected $cursos;

    public function __construct()
    {
        $this->cursos = Curso::join('tb_area_curso', 'tb_cursos.area', '=', 'tb_area_curso.id')
            ->select(
                'tb_cursos.id as id',
                'tb_cursos.nome as nome',
                'tb_cursos.duracao as duracao',
                'tb_cursos.valor as valor',
                'tb_cursos.inscricao as inscricao',
                'tb_cursos.desconto as desconto',
                'tb_cursos.objetivo as objetivo',
                'tb_cursos.publico as publico',
                'tb_cursos.requisitos as requisitos',
                'tb_cursos.material as material',
                'tb_cursos.cartao as cartao',
                'tb_cursos.cartao_parcelas AS cartao_parcelas',
                'tb_cursos.cartao_valor_total AS cartao_valor_total',
                'tb_area_curso.nome AS area_nome',
                'tb_area_curso.id AS area_id'
            );
    }

    public function extension()
    {
        $cursos = $this->cursos
            ->where('tb_cursos.tipo', 4)
            ->where('tb_cursos.site', 1)
            ->orderBy('tb_cursos.nome', 'asc')
            ->get();
        foreach ($cursos as $curso) {
            $result = \DB::select(
                \DB::raw(
                    "SELECT nome
                    FROM tb_disciplinas
                    WHERE id_curso ={$curso->id}
                    AND tipo IN (1,3,4) AND ativo = 1
                    ORDER BY ordem ASC"
                )
            );
            $curso->estrutura = $result;
            $curso->hasDesconto = $curso->desconto != 0 ? true : false;
        }
        $areas = $cursos->groupBy('area_nome')->sort();

        return view('customer.pages.extension.index')
            ->with('areas', $areas);
    }
    public function graduation()
    {
        return view('customer.pages.graduation.index');
    }
    public function posGraduation()
    {
        return view('customer.pages.pos-graduation.index');
    }
    public function presencial()
    {
        $cursos = $this->cursos
            ->where('tb_cursos.tipo', 1)
            ->where('tb_cursos.site', 1)
            ->orderBy('tb_cursos.nome', 'asc')
            ->get();
        foreach ($cursos as $curso) {
            $result = \DB::select(
                \DB::raw(
                    "SELECT nome
                        FROM tb_disciplinas
                        WHERE id_curso ={$curso->id}
                        AND tipo IN (1,3,4) AND ativo = 1
                        ORDER BY ordem ASC"
                )
            );
            $curso->estrutura = $result;
            $curso->hasDesconto = $curso->desconto != 0 ? true : false;
        }
        $areas = $cursos->groupBy('area_nome')->sort();
        return view('customer.pages.pos-graduation.presencial')
            ->with('areas', $areas);
    }
    public function professionalizing()
    {
        $cursos = $this->cursos
            ->whereIn('tb_cursos.tipo', [8, 9, 10])
            ->where('tb_cursos.site', 1)
            ->orderBy('tb_cursos.nome', 'asc')
            ->get();
        foreach ($cursos as $curso) {
            $result = \DB::select(
                \DB::raw(
                    "SELECT nome
                            FROM tb_disciplinas
                            WHERE id_curso ={$curso->id}
                            AND tipo IN (1,3,4) AND ativo = 1
                            ORDER BY ordem ASC"
                )
            );
            $curso->estrutura = $result;
            $curso->hasDesconto = $curso->desconto != 0 ? true : false;
        }
        $areas = $cursos->groupBy('area_nome')->sort();
        return view('customer.pages.professionalizing.index')
            ->with('areas', $areas);
    }
    public function computing()
    {
        $cursos = $this->cursos
            ->where('tb_cursos.tipo', 11)
            ->where('tb_cursos.site', 1)
            ->orderBy('tb_cursos.nome', 'asc')
            ->get();
        $areas = $cursos->groupBy('area_nome')->sort();
        return view('customer.pages.computing.index')
            ->with('areas', $areas);
    }
    public function workshop()
    {
        $cursos = $this->cursos
            ->where('tb_cursos.tipo', 12)
            ->where('tb_cursos.site', 1)
            ->orderBy('tb_cursos.nome', 'asc')
            ->get();

        foreach ($cursos as $curso) {
            $result = \DB::select(
                \DB::raw(
                    "SELECT SUM(carga) AS total
                        FROM tb_disciplinas
                        WHERE id_curso = {$curso->id}
                        AND tipo IN (1,3,4) AND ativo = 1
                        GROUP BY ordem
                        ORDER BY ordem ASC"
                )
            );
            $curso->total = isset($result[0]) ? $result[0]->total : 0;
            $curso->hasDesconto = $curso->desconto != 0 ? true : false;
        }

        $areas = $cursos->groupBy('area_nome')->sort();
        return view('customer.pages.workshop.index')
            ->with('areas', $areas);
    }
}
