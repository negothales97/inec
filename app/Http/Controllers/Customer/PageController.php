<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Polo;

class PageController extends Controller
{
    public function dataCompany()
    {
        return view('customer.pages.company.data');
    }
    public function study()
    {
        $polos = Polo::join('tb_cidades', 'tb_cidades.id', '=', 'tb_polos.cidade')
            ->where('tb_polos.ativo', 1)
            ->orderBy('tb_cidades.nome', "asc")
            ->get();
        return view('customer.pages.study.index')
            ->with('polos', $polos);
    }
    public function company()
    {
        return view('customer.pages.company.index');
    }
}
