<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\SubscriptionRequest;

class SubscriptionController extends Controller
{
    public function index()
    {
        return view('customer.pages.subscription.index');
    }

    public function send(SubscriptionRequest $request)
    {
        $data=  $request->all();

        Mail::send('email.subscription', ['data' => $data], function ($m)
             use ($data) {
                $m->from('webmaster@inecontinuada.com.br', 'INEC');
                $m->to('direcao@inecontinuada.com.br', 'Direção INEC')->subject('Inscrição no site');
            });

        return redirect()
            ->back()
            ->with('success', 'Email enviado com sucesso');
    }
}
