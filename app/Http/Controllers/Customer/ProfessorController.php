<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Professor;

class ProfessorController extends Controller
{
    public function index()
    {
        $professores = Professor::where('ativo', 1)
            ->where('atualizado', 1)
            ->select('id', 'nome', 'escolaridade', 'ativo')
            ->orderBy('nome', 'asc')
            ->get();

        return view('customer.pages.teacher.index')
            ->with('professores', $professores);
    }
}
